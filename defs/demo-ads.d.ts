declare namespace innovid.iroll.demo {
    const FRAMEWORK_IROLL = "innovid";
    class Ads implements SimpleVideoPlayerHandler {
        currentAd: AdInstance;
        currentAds: AdInstance[];
        currentAdPod: AdPod;
        app: DemoApp;
        constructor(app: DemoApp);
        handlePlaybackProgress(position: number): void;
        handlePlaybackStateChanged(state: PlaybackState, oldState?: PlaybackState): void;
        log(msg: string, data?: any): void;
        private _processCurrentAds(position);
        private _initAds(adPod);
        private _resetCurrentAdPod();
        private _setupCurrentAdPod(adPod);
        private _createAd(adInfo);
        private _startAd(adInstance);
        private _resetAd(adInstance);
        private _preloadAd(adInstance);
        private _getAdPodForPosition(position, offset?);
    }
    interface AdInstance {
        started: boolean;
        running: boolean;
        startAt: number;
        duration: number;
        framework: string;
        start(): void;
        stop(): void;
        load(): void;
        dispose(): void;
        processPlaybackStateChanged(video: SimpleVideoPlayer): void;
    }
}
