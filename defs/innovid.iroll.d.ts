declare namespace innovid.iroll {
    function create(tagUrl: string, params?: IrollAdParameters): IrollAd;
    interface IrollAd {
        init(container: HTMLElement, handler?: IrollAdHandler): void;
        load(): void;
        start(): void;
        stop(): void;
        skip(reason: string): void;
        injectKeyboardEvent(evt: RemoteControlEvent): void;
        injectPlaybackInfo(info: PlaybackInfo): void;
    }
    interface IrollAdParameters {
        keyMap?: {
            UP: number;
            DOWN: number;
            LEFT: number;
            RIGHT: number;
            ENTER: number;
            BACK: number;
            OPTIONS?: number;
            PLAY_PAUSE?: number;
            FF?: number;
            RW?: number;
        };
        useExternalRemote?: boolean;
        autoStart?: boolean;
        platform?: string;
        advertisingId?: string;
    }
    interface IrollAdHandler {
        handleIrollEvent?(type: IrollAdEvent, data?: any): any;
        handleIrollPlaybackRequest?(type: IrollPlaybackRequest, data?: any): any;
    }
    type IrollAdEvent = 'iroll-prepared' | 'iroll-ready' | 'iroll-started' | 'iroll-impression' | 'iroll-expand' | 'iroll-collapse' | 'iroll-ended' | 'iroll-failed' | 'iroll-video-progress' | 'iroll-video-started' | 'iroll-video-first-quartile' | 'iroll-video-midpoint' | 'iroll-video-third-quartile' | 'iroll-video-completed' | 'iroll-skip-ad-request' | 'iroll-overlay-close';
    class IrollAdEvents {
        static readonly IMP: IrollAdEvent;
        static readonly PREPARED: IrollAdEvent;
        static readonly READY: IrollAdEvent;
        static readonly STARTED: IrollAdEvent;
        static readonly ENDED: IrollAdEvent;
        static readonly FAILED: IrollAdEvent;
        static readonly EXPAND: IrollAdEvent;
        static readonly COLLAPSE: IrollAdEvent;
        static readonly VIDEO_PROGRESS: IrollAdEvent;
        static readonly VIDEO_STARTED: IrollAdEvent;
        static readonly VIDEO_FIRST_QUARTILE: IrollAdEvent;
        static readonly VIDEO_MIDPOINT: IrollAdEvent;
        static readonly VIDEO_THIRD_QUARTILE: IrollAdEvent;
        static readonly VIDEO_COMPLETED: IrollAdEvent;
        static readonly SKIP_AD_REQUEST: IrollAdEvent;
        static readonly OVERLAY_CLOSE: IrollAdEvent;
    }
    type PlaybackState = 'stopped' | 'paused' | 'playing';
    class PlaybackStates {
        static readonly STOPPED: PlaybackState;
        static readonly PLAYING: PlaybackState;
        static readonly PAUSED: PlaybackState;
    }
    type IrollPlaybackRequest = 'iroll-request-playback-pause' | 'iroll-request-playback-resume' | 'iroll-request-playback-restart-on-resume';
    class IrollPlaybackRequests {
        static readonly PLAYBACK_PAUSE: IrollPlaybackRequest;
        static readonly PLAYBACK_RESUME: IrollPlaybackRequest;
        static readonly PLAYBACK_RESTART_ON_RESUME: IrollPlaybackRequest;
        static isRequest(type: string): boolean;
    }
    interface RemoteControlEvent {
        type: RemoteControlEventType;
        keyCode: number;
        preventDefault(): void;
        stopPropagation(): void;
        stopImmediatePropagation?(): void;
    }
    type RemoteControlEventType = 'keydown' | 'keyup';
    function randomUUID(): string;
    interface PlaybackInfo {
        playbackState: PlaybackState;
        position: number;
        startAt: number;
        ended: boolean;
    }
}
