import PlaybackState = innovid.iroll.PlaybackState;
declare namespace innovid.iroll.demo {
    function run(): void;
    class DemoApp {
        private _btnStartEl;
        private _aEl;
        private _ads;
        private _playbackStarted;
        video: SimpleVideoPlayer;
        videoEl: HTMLVideoElement;
        containerEl: HTMLElement;
        el: HTMLElement;
        cfg: DemoCfg;
        run(cfg: DemoCfg, el: HTMLElement): void;
        private _prepareUI();
        private _preparePlayback();
        _startPlayback(): void;
        private _handleKeyboardEvent;
        private _handleFocusEvent;
    }
    class SimpleVideoPlayer {
        el: HTMLVideoElement;
        mf: MediaFile;
        playbackState: PlaybackState;
        position: number;
        duration: number;
        ended: boolean;
        restartRequested: boolean;
        restarting: boolean;
        restartPosition: number;
        handler: SimpleVideoPlayerHandler;
        private pendingPlayRequest;
        private pendingSeekRequest;
        constructor(el: HTMLVideoElement);
        setup(mf: MediaFile, handler?: SimpleVideoPlayerHandler): void;
        start(): void;
        resume(): boolean;
        pause(): boolean;
        requestRestartOnResume(): void;
        log(msg: string, data?: any): void;
        private _setPlaybackState(state);
        private _restartPlayback();
        private _processPlaybackStateChanged(newState, oldState?);
        private _handleCanPlay;
        private _handlePlaybackStateChanged;
        private _handlePlaybackProgress;
        private _handlePlaybackCompleted;
    }
    interface SimpleVideoPlayerHandler {
        handlePlaybackProgress(position: number): void;
        handlePlaybackStateChanged(state: PlaybackState, oldState?: PlaybackState): void;
    }
    interface DemoCfg {
        content: MediaFile;
        adPods: AdPod[];
    }
    interface MediaFile {
        framework?: string;
        duration: number;
        url: string;
        mimetype: string;
    }
    interface AdPod {
        renderTime: number;
        duration: number;
        ads: AdInfo[];
    }
    interface AdInfo {
        id: string;
        duration: number;
        framework?: string;
        companions: CompanionAdInfo[];
    }
    interface CompanionAdInfo {
        url: string;
        mimetype: string;
    }
}
