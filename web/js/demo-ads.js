var innovid;
(function (innovid) {
    var iroll;
    (function (iroll) {
        var demo;
        (function (demo) {
            demo.FRAMEWORK_IROLL = 'innovid';
            var Ads = /** @class */ (function () {
                function Ads(app) {
                    this.app = app;
                }
                Ads.prototype.handlePlaybackProgress = function (position) {
                    // this.log(`handlePlaybackProgress(-${position}-)`);
                    // notify the current ad about video playback progress
                    if (this.currentAd) {
                        this.currentAd.processPlaybackStateChanged(this.app.video);
                    }
                    var adPod;
                    if (this.currentAdPod) {
                        // check current adPod
                        if (position >= (this.currentAdPod.renderTime + this.currentAdPod.duration)) {
                            this._resetCurrentAdPod();
                        }
                    }
                    else {
                        // we will initialize AdPod's ads 1 sec before
                        adPod = this._getAdPodForPosition(position, 1);
                        if (adPod) {
                            this._setupCurrentAdPod(adPod);
                        }
                    }
                    if (this.currentAdPod) {
                        this._processCurrentAds(position);
                    }
                    else if (this.currentAd && AdTimeUtil.shouldBeStopped(this.currentAd, position)) {
                        this._resetAd(this.currentAd);
                    }
                };
                Ads.prototype.handlePlaybackStateChanged = function (state, oldState) {
                };
                Ads.prototype.log = function (msg, data) {
                    console.log("Demo / Ads # " + msg, data);
                };
                Ads.prototype._processCurrentAds = function (position) {
                    for (var _i = 0, _a = this.currentAds; _i < _a.length; _i++) {
                        var ad = _a[_i];
                        if (AdTimeUtil.shouldBeStarted(ad, position)) {
                            this._startAd(ad);
                        }
                        else if (AdTimeUtil.shouldBePreloaded(ad, position)) {
                            this._preloadAd(ad);
                        }
                        else if (AdTimeUtil.shouldBeStopped(ad, position)) {
                            this._resetAd(ad);
                        }
                    }
                };
                Ads.prototype._initAds = function (adPod) {
                    var ads = [];
                    var renderTime = adPod.renderTime;
                    this.log('_initAds()', adPod);
                    for (var _i = 0, _a = adPod.ads; _i < _a.length; _i++) {
                        var adInfo = _a[_i];
                        var adInstance = this._createAd(adInfo);
                        adInstance.startAt = renderTime;
                        renderTime += adInstance.duration;
                        ads.push(adInstance);
                    }
                    this.currentAds = ads;
                };
                Ads.prototype._resetCurrentAdPod = function () {
                    if (!this.currentAdPod) {
                        return;
                    }
                    this.log('_resetCurrentAdPod()');
                    if (this.currentAds) {
                        for (var _i = 0, _a = this.currentAds; _i < _a.length; _i++) {
                            var ad = _a[_i];
                            ad.dispose();
                        }
                    }
                    this.currentAds = null;
                    this.currentAdPod = null;
                };
                Ads.prototype._setupCurrentAdPod = function (adPod) {
                    var ads = [];
                    var renderTime = adPod.renderTime;
                    this.log('_setupCurrentAdPod()', adPod);
                    for (var _i = 0, _a = adPod.ads; _i < _a.length; _i++) {
                        var adInfo = _a[_i];
                        var adInstance = this._createAd(adInfo);
                        adInstance.startAt = renderTime;
                        renderTime += adInstance.duration;
                        ads.push(adInstance);
                    }
                    this.currentAds = ads;
                    this.currentAdPod = adPod;
                };
                Ads.prototype._createAd = function (adInfo) {
                    if (adInfo.framework != demo.FRAMEWORK_IROLL) {
                        return null;
                    }
                    return new IrollAdInstance(adInfo, this.app.containerEl, this.app.video);
                };
                Ads.prototype._startAd = function (adInstance) {
                    this.log('_startAd()', adInstance);
                    this.currentAd = adInstance;
                    adInstance.start();
                };
                Ads.prototype._resetAd = function (adInstance) {
                    this.log('_disposeAd()', adInstance);
                    if (adInstance.running) {
                        adInstance.stop();
                    }
                };
                Ads.prototype._preloadAd = function (adInstance) {
                    this.log('_preloadAd()', adInstance);
                    adInstance.load();
                };
                Ads.prototype._getAdPodForPosition = function (position, offset) {
                    if (offset === void 0) { offset = 1; }
                    for (var _i = 0, _a = this.app.cfg.adPods; _i < _a.length; _i++) {
                        var adPod = _a[_i];
                        if ((position >= (adPod.renderTime - offset)) && position < (adPod.renderTime + adPod.duration)) {
                            return adPod;
                        }
                    }
                    return null;
                };
                return Ads;
            }());
            demo.Ads = Ads;
            var IrollAdInstance = /** @class */ (function () {
                function IrollAdInstance(info, container, player) {
                    this._container = container;
                    this._info = info;
                    this._player = player;
                    this.duration = info.duration;
                    this.framework = info.framework;
                    this.started = false;
                }
                IrollAdInstance.prototype.start = function () {
                    this.log('start()');
                    if (this.started && this.running) {
                        return;
                    }
                    this.load();
                    this.running = true;
                    this.started = true;
                    this.iroll.start();
                };
                IrollAdInstance.prototype.stop = function () {
                    this.log('stop()');
                    this.running = false;
                    if (this.iroll) {
                        this.iroll.stop();
                    }
                };
                IrollAdInstance.prototype.load = function () {
                    this.log('load()');
                    if (this.started) {
                        return;
                    }
                    this.running = false;
                    this.started = true;
                    this.iroll = innovid.iroll.create(this._info.companions[0].url, this.getParameters());
                    this.iroll.init(this._container, this);
                    this.iroll.load();
                };
                IrollAdInstance.prototype.dispose = function () {
                    this.log('dispose()');
                };
                IrollAdInstance.prototype.handleIrollEvent = function (type, data) {
                    // app special handling
                    switch (type) {
                        case iroll.IrollAdEvents.OVERLAY_CLOSE:
                            window.focus();
                            break;
                    }
                };
                IrollAdInstance.prototype.handleIrollPlaybackRequest = function (type, data) {
                    switch (type) {
                        case iroll.IrollPlaybackRequests.PLAYBACK_RESUME:
                            this._player.resume();
                            break;
                        case iroll.IrollPlaybackRequests.PLAYBACK_PAUSE:
                            this._player.pause();
                            break;
                        case iroll.IrollPlaybackRequests.PLAYBACK_RESTART_ON_RESUME:
                            this._player.requestRestartOnResume();
                            break;
                    }
                };
                IrollAdInstance.prototype.processPlaybackStateChanged = function (video) {
                    if (this.iroll) {
                        this.iroll.injectPlaybackInfo({
                            startAt: this.startAt,
                            playbackState: video.playbackState,
                            position: video.position - this.startAt,
                            ended: video.ended
                        });
                    }
                };
                IrollAdInstance.prototype.log = function (msg, data) {
                    console.log("Demo / Ad # " + msg, data);
                };
                IrollAdInstance.prototype.getParameters = function () {
                    return {
                        keyMap: {
                            UP: 40,
                            LEFT: 37,
                            RIGHT: 39,
                            DOWN: 38,
                            ENTER: 32,
                            BACK: 8 // BACK
                        },
                        platform: "dev",
                        advertisingId: UUID.create()
                    };
                };
                return IrollAdInstance;
            }());
            var AdTimeUtil = /** @class */ (function () {
                function AdTimeUtil() {
                }
                AdTimeUtil.shouldBeStarted = function (ad, position) {
                    return ad.started && !ad.running && (position - ad.startAt) >= 0 && position < (ad.startAt + ad.duration);
                };
                AdTimeUtil.shouldBePreloaded = function (ad, position) {
                    if (ad.started || ad.framework != demo.FRAMEWORK_IROLL) {
                        return false;
                    }
                    var dt = ad.startAt - position;
                    return dt >= 0 && dt < 1;
                };
                AdTimeUtil.shouldBeStopped = function (ad, position) {
                    return ad.running === true && (position - ad.startAt) > (ad.duration + 1);
                };
                return AdTimeUtil;
            }());
            var UUID = /** @class */ (function () {
                function UUID() {
                }
                UUID.create = function () {
                    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                        return v.toString(16);
                    });
                };
                return UUID;
            }());
        })(demo = iroll.demo || (iroll.demo = {}));
    })(iroll = innovid.iroll || (innovid.iroll = {}));
})(innovid || (innovid = {}));
