var innovid;
(function (innovid) {
    var iroll;
    (function (iroll) {
        function create(tagUrl, params) {
            return new IrollAdWrapper(tagUrl, params);
        }
        iroll.create = create;
        var IrollAdEvents = /** @class */ (function () {
            function IrollAdEvents() {
            }
            IrollAdEvents.IMP = 'iroll-impression';
            IrollAdEvents.PREPARED = 'iroll-prepared';
            IrollAdEvents.READY = 'iroll-ready';
            IrollAdEvents.STARTED = 'iroll-started';
            IrollAdEvents.ENDED = 'iroll-ended';
            IrollAdEvents.FAILED = 'iroll-failed';
            IrollAdEvents.EXPAND = 'iroll-expand';
            IrollAdEvents.COLLAPSE = 'iroll-collapse';
            IrollAdEvents.VIDEO_PROGRESS = 'iroll-video-progress';
            IrollAdEvents.VIDEO_STARTED = 'iroll-video-started';
            IrollAdEvents.VIDEO_FIRST_QUARTILE = 'iroll-video-first-quartile';
            IrollAdEvents.VIDEO_MIDPOINT = 'iroll-video-midpoint';
            IrollAdEvents.VIDEO_THIRD_QUARTILE = 'iroll-video-third-quartile';
            IrollAdEvents.VIDEO_COMPLETED = 'iroll-video-completed';
            IrollAdEvents.SKIP_AD_REQUEST = 'iroll-skip-ad-request';
            IrollAdEvents.OVERLAY_CLOSE = 'iroll-overlay-close';
            return IrollAdEvents;
        }());
        iroll.IrollAdEvents = IrollAdEvents;
        var PlaybackStates = /** @class */ (function () {
            function PlaybackStates() {
            }
            PlaybackStates.STOPPED = 'stopped';
            PlaybackStates.PLAYING = 'playing';
            PlaybackStates.PAUSED = 'paused';
            return PlaybackStates;
        }());
        iroll.PlaybackStates = PlaybackStates;
        var IrollPlaybackRequests = /** @class */ (function () {
            function IrollPlaybackRequests() {
            }
            IrollPlaybackRequests.isRequest = function (type) {
                return type == IrollPlaybackRequests.PLAYBACK_PAUSE
                    || type == IrollPlaybackRequests.PLAYBACK_RESUME
                    || type == IrollPlaybackRequests.PLAYBACK_RESTART_ON_RESUME;
            };
            IrollPlaybackRequests.PLAYBACK_PAUSE = 'iroll-request-playback-pause';
            IrollPlaybackRequests.PLAYBACK_RESUME = 'iroll-request-playback-resume';
            IrollPlaybackRequests.PLAYBACK_RESTART_ON_RESUME = 'iroll-request-playback-restart-on-resume';
            return IrollPlaybackRequests;
        }());
        iroll.IrollPlaybackRequests = IrollPlaybackRequests;
        var log = function (msg, data) {
            console.log(msg, data);
        };
        var elA = document.createElement('a');
        var IrollAdWrapper = /** @class */ (function () {
            function IrollAdWrapper(tagUrl, params) {
                var _this = this;
                this.started = false;
                this.disposed = false;
                this.position = -1;
                this.duration = -1;
                this.playbackState = PlaybackStates.STOPPED;
                this._handleIncomingMessage = function (evt) {
                    if (!evt.data) {
                        return;
                    }
                    var eType = evt.data['type'] + '';
                    var eData = evt.data['params'];
                    if (IrollPlaybackRequests.isRequest(eType)) {
                        _this.handler.handleIrollPlaybackRequest(eType, eData);
                    }
                    else {
                        if (eType == IrollAdEvents.VIDEO_PROGRESS && !isNaN(eData['duration'])) {
                            _this.position = eData['position'];
                            _this.duration = eData['duration'];
                        }
                        // this.log(`handleIncomingMessage("${evt.type}", "${eType}", ${JSON.stringify( eData )})`);
                        if (eType == IrollAdEvents.ENDED || eType == IrollAdEvents.FAILED) {
                            _this.playbackState = PlaybackStates.STOPPED;
                            window.removeEventListener("message", _this._handleIncomingMessage);
                        }
                        _this._fireIrollEvent(eType, eData);
                    }
                };
                this.tagUrl = tagUrl;
                this.params = params;
                this.uuid = randomUUID();
            }
            IrollAdWrapper.prototype.init = function (container, handler) {
                this.container = container;
                this.handler = new HandlerWrapper(handler);
            };
            IrollAdWrapper.prototype.load = function () {
                this.log('load()');
                this._createIframe();
            };
            IrollAdWrapper.prototype.start = function () {
                if (this.started) {
                    return;
                }
                this.log('start()');
                this.started = true;
                this.playbackState = PlaybackStates.PLAYING;
                // create iframe
                this._createIframe(true);
                // fire @prepared event
                this._fireDeferredIrollEvent(IrollAdEvents.PREPARED);
                // setup message listener
                window.addEventListener("message", this._handleIncomingMessage);
            };
            IrollAdWrapper.prototype.dispose = function () {
                if (this.disposed) {
                    return;
                }
                log('dispose()');
                // reset handler
                this.handler = null;
                // remove listener
                window.removeEventListener("message", this._handleIncomingMessage);
                // detach view
                this._disposeIframeEl();
                // mark as disposed
                this.disposed = true;
            };
            IrollAdWrapper.prototype.stop = function () {
                log('stop()');
            };
            IrollAdWrapper.prototype.skip = function (reason) {
                this.log("skip(reason: " + reason + ")");
                this._send2iframe({ type: 'skip', reason: reason });
            };
            IrollAdWrapper.prototype.injectKeyboardEvent = function (evt) {
                this._send2iframe({ type: evt.type, keyCode: evt.keyCode });
            };
            IrollAdWrapper.prototype.injectPlaybackInfo = function (info) {
                this._send2iframe({ type: 'playback-update', data: info });
            };
            IrollAdWrapper.prototype._send2iframe = function (data) {
                try {
                    this.el.contentWindow.postMessage(data, '*');
                }
                catch (e) {
                    log('_send2iframe() -- Error! -- ', e);
                }
            };
            IrollAdWrapper.prototype._fireIrollEvent = function (eType, eData) {
                this.handler.handleIrollEvent(eType, eData);
            };
            ;
            IrollAdWrapper.prototype._fireDeferredIrollEvent = function (eType, eData) {
                var _this = this;
                var id = setTimeout(function () {
                    clearTimeout(id);
                    _this._fireIrollEvent(eType, eData);
                }, 0);
            };
            ;
            IrollAdWrapper.prototype._prepareAdUrl = function (url, autoStart) {
                var params = this.params || {};
                params.autoStart = autoStart;
                elA.href = url;
                elA.hash = 'params=' + encodeURIComponent(JSON.stringify(params));
                return elA.href;
            };
            IrollAdWrapper.prototype._createIframe = function (autoStart) {
                if (autoStart === void 0) { autoStart = false; }
                this.log('_createIframe()', this.el);
                if (!this.el) {
                    this._prepareIframeEl(this._prepareAdUrl(this.tagUrl, autoStart));
                }
            };
            IrollAdWrapper.prototype._prepareIframeEl = function (adUrl) {
                var el;
                el = document.createElement('iframe');
                el.setAttribute('className', 'innovid-iroll');
                el.setAttribute('id', this.uuid);
                el.setAttribute('src', adUrl);
                el.style.zIndex = 1000;
                el.style.border = '0px';
                el.style.position = "absolute";
                el.style.background = "transparent";
                el.style.left = "0px";
                el.style.top = "0px";
                el.style.width = "100%";
                el.style.height = "100%";
                this.container.appendChild(el);
                // save ref
                this.el = el;
            };
            ;
            IrollAdWrapper.prototype._disposeIframeEl = function () {
                if (!this.el) {
                    return;
                }
                try {
                    this.container.removeChild(this.el);
                }
                catch (e) { }
                // reset value
                this.el.setAttribute('src', 'about:blank');
                this.el = null;
            };
            IrollAdWrapper.prototype.log = function (msg, data) {
                log("### iroll-wrapper / Ad(" + this.uuid + ") # " + msg, data);
            };
            return IrollAdWrapper;
        }());
        function randomUUID() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }
        iroll.randomUUID = randomUUID;
        var HandlerWrapper = /** @class */ (function () {
            function HandlerWrapper(handler) {
                this.handler = handler;
            }
            HandlerWrapper.prototype.handleIrollEvent = function (type, data) {
                if (HandlerWrapper._isMethodExists(this.handler, 'handleIrollEvent')) {
                    this.handler.handleIrollEvent(type, data);
                }
            };
            HandlerWrapper.prototype.handleIrollPlaybackRequest = function (type, data) {
                if (HandlerWrapper._isMethodExists(this.handler, 'handleIrollPlaybackRequest')) {
                    this.handler.handleIrollPlaybackRequest(type, data);
                }
            };
            HandlerWrapper._isMethodExists = function (o, methodName) {
                return (o && Object.prototype.toString.apply(o[methodName]) == '[object Function]');
            };
            return HandlerWrapper;
        }());
    })(iroll = innovid.iroll || (innovid.iroll = {}));
})(innovid || (innovid = {}));
