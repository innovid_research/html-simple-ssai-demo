var innovid;
(function (innovid) {
    var iroll;
    (function (iroll) {
        var demo;
        (function (demo) {
            function run() {
                (new DemoApp()).run(JSON.parse(document.querySelector('#-demo-data-').textContent), document.querySelector('#-demo-player'));
            }
            demo.run = run;
            var DemoApp = /** @class */ (function () {
                function DemoApp() {
                    var _this = this;
                    this._handleKeyboardEvent = function (evt) {
                        console.log("Demo # _handleKeydown(" + evt.keyCode + ", prevented: " + evt.defaultPrevented + ")");
                        evt.preventDefault();
                        if (!_this._playbackStarted) {
                            _this._startPlayback();
                        }
                    };
                    this._handleFocusEvent = function (evt) {
                        console.log("Demo # _handleFocusEvent(" + evt.type + ")", document.activeElement);
                    };
                }
                DemoApp.prototype.run = function (cfg, el) {
                    console.log('Demo # run()');
                    this.cfg = cfg;
                    this.el = el;
                    this._prepareUI();
                    this._preparePlayback();
                };
                DemoApp.prototype._prepareUI = function () {
                    var _this = this;
                    console.log('Demo # _prepareUI()');
                    this.videoEl = this.el.querySelector('#-video');
                    this.containerEl = this.el.querySelector('#-iroll-ad-placeholder');
                    this._btnStartEl = this.el.querySelector('#-btn-start');
                    this._btnStartEl.onclick = function () {
                        _this._startPlayback();
                    };
                    window.addEventListener('keydown', this._handleKeyboardEvent);
                    window.addEventListener('focusin', this._handleFocusEvent);
                    window.addEventListener('focusout', this._handleFocusEvent);
                    this._aEl = document.querySelector('#-key-handler-');
                    this._aEl.focus();
                };
                DemoApp.prototype._preparePlayback = function () {
                    console.log('Demo # _preparePlaylist()');
                    this._ads = new demo.Ads(this);
                    this.video = new SimpleVideoPlayer(this.videoEl);
                    this.video.setup(this.cfg.content, this._ads);
                };
                DemoApp.prototype._startPlayback = function () {
                    console.log('Demo # _startPlayback()');
                    this._playbackStarted = true;
                    // hide start playback button
                    this._btnStartEl.style.display = 'none';
                    // start playback
                    this.video.start();
                };
                return DemoApp;
            }());
            demo.DemoApp = DemoApp;
            var SimpleVideoPlayer = /** @class */ (function () {
                function SimpleVideoPlayer(el) {
                    var _this = this;
                    this.playbackState = iroll.PlaybackStates.STOPPED;
                    this.position = -1;
                    this.duration = -1;
                    this.pendingPlayRequest = false;
                    this.pendingSeekRequest = false;
                    this._handleCanPlay = function () {
                        if (_this.pendingPlayRequest === true) {
                            _this.pendingPlayRequest = false;
                            if (_this.restarting && _this.restartPosition > 0) {
                                _this.el.currentTime = _this.restartPosition;
                            }
                            else {
                                _this.el.play();
                            }
                        }
                    };
                    this._handlePlaybackStateChanged = function () {
                    };
                    this._handlePlaybackProgress = function () {
                        if (_this.position == _this.el.currentTime) {
                            return;
                        }
                        // !important - ignore playback progress when video is restarting
                        if (_this.restarting) {
                            if (_this.playbackState == iroll.PlaybackStates.PAUSED && (_this.el.currentTime + 0.05) >= _this.restartPosition) {
                                // reset flags
                                _this.restarting = false;
                                _this.restartPosition = -1;
                                _this.el.play();
                            }
                        }
                        if (!_this.restarting) {
                            // save position
                            _this.position = _this.el.currentTime;
                            // notify handler
                            if (_this.handler) {
                                _this.handler.handlePlaybackProgress(_this.position);
                            }
                        }
                    };
                    this._handlePlaybackCompleted = function () {
                    };
                    this.el = el;
                }
                SimpleVideoPlayer.prototype.setup = function (mf, handler) {
                    this.log('start()', mf);
                    this.mf = mf;
                    this.handler = handler;
                    this.el.autoplay = false;
                    this.el.preload = 'none';
                    this.el.loop = false;
                    this.position = 0;
                    this.duration = mf.duration;
                    this.el.src = mf.url;
                };
                SimpleVideoPlayer.prototype.start = function () {
                    this.log('start()');
                    this.el.addEventListener('canplay', this._handleCanPlay);
                    this.el.addEventListener('loadmetadata', this._handleCanPlay);
                    this.el.addEventListener('timeupdate', this._handlePlaybackProgress);
                    this.el.addEventListener('ended', this._handlePlaybackCompleted);
                    this.el.addEventListener('pause', this._handlePlaybackStateChanged);
                    this.el.addEventListener('play', this._handlePlaybackStateChanged);
                    this.el.addEventListener('playing', this._handlePlaybackStateChanged);
                    this.pendingPlayRequest = true;
                    this.el.load();
                    this._setPlaybackState(iroll.PlaybackStates.PLAYING);
                };
                SimpleVideoPlayer.prototype.resume = function () {
                    if (this.playbackState != iroll.PlaybackStates.PAUSED) {
                        return false;
                    }
                    this.log("resume() -- is-restart-requested: " + this.restartRequested);
                    if (this.restartRequested) {
                        this._restartPlayback();
                    }
                    else {
                        this._setPlaybackState(iroll.PlaybackStates.PLAYING);
                        this.el.play();
                    }
                };
                SimpleVideoPlayer.prototype.pause = function () {
                    if (this.playbackState != iroll.PlaybackStates.PLAYING) {
                        return false;
                    }
                    this.log("pause()");
                    this.el.pause();
                    this._setPlaybackState(iroll.PlaybackStates.PAUSED);
                    return true;
                };
                SimpleVideoPlayer.prototype.requestRestartOnResume = function () {
                    if (this.playbackState != iroll.PlaybackStates.PAUSED) {
                        return;
                    }
                    this.log("requestRestartOnResume()");
                    // save position and change flag
                    this.restartRequested = true;
                    this.restartPosition = this.position;
                    // stopping playback w/o changing #playbackState
                    this.el.pause(); // ensure
                    this.el.removeAttribute('src');
                };
                SimpleVideoPlayer.prototype.log = function (msg, data) {
                    console.log("Demo / Video # " + msg, data);
                };
                SimpleVideoPlayer.prototype._setPlaybackState = function (state) {
                    var oldState = this.playbackState;
                    if (oldState == state) {
                        return false;
                    }
                    this.playbackState = state;
                    this._processPlaybackStateChanged(state, oldState);
                    return true;
                };
                SimpleVideoPlayer.prototype._restartPlayback = function () {
                    if (this.restarting || !this.restartRequested) {
                        return false;
                    }
                    this.log("_restartPlayback(position: " + this.restartPosition + ")");
                    this.restarting = true;
                    this.restartRequested = false;
                    this.pendingPlayRequest = true;
                    this.el.src = this.mf.url;
                    this.el.load();
                    return true;
                };
                SimpleVideoPlayer.prototype._processPlaybackStateChanged = function (newState, oldState) {
                    this.log("processPlaybackStateChanged(" + newState + ", old: " + oldState);
                    if (this.handler) {
                        this.handler.handlePlaybackStateChanged(newState, oldState);
                    }
                };
                return SimpleVideoPlayer;
            }());
            demo.SimpleVideoPlayer = SimpleVideoPlayer;
        })(demo = iroll.demo || (iroll.demo = {}));
    })(iroll = innovid.iroll || (innovid.iroll = {}));
})(innovid || (innovid = {}));
