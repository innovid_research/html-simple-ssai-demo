namespace innovid.iroll {
  export function create(tagUrl: string, params?: IrollAdParameters):IrollAd {
    return new IrollAdWrapper(tagUrl, params);
  }

  export interface IrollAd {
    init(container: HTMLElement, handler?: IrollAdHandler): void;
    load(): void;
    start(): void;
    stop(): void;
    skip(reason: string): void;

    injectKeyboardEvent(evt: RemoteControlEvent): void;
    injectPlaybackInfo(info: PlaybackInfo): void;
  }

  export interface IrollAdParameters {
    keyMap? : {
      UP          : number,
      DOWN        : number,
      LEFT        : number,
      RIGHT       : number,
      ENTER       : number,
      BACK        : number,
      OPTIONS?    : number,
      PLAY_PAUSE? : number,
      FF?         : number,
      RW?         : number
    };

    useExternalRemote? : boolean;
    autoStart?         : boolean;
    platform?          : string;
    advertisingId?     : string;
  }

  export interface IrollAdHandler {
    handleIrollEvent?(type: IrollAdEvent, data?: any ): any;
    handleIrollPlaybackRequest?(type: IrollPlaybackRequest, data?: any): any;
  }

  export type IrollAdEvent
    = 'iroll-prepared'
    | 'iroll-ready'
    | 'iroll-started'
    | 'iroll-impression'
    | 'iroll-expand'
    | 'iroll-collapse'
    | 'iroll-ended'
    | 'iroll-failed'
    | 'iroll-video-progress'
    | 'iroll-video-started'
    | 'iroll-video-first-quartile'
    | 'iroll-video-midpoint'
    | 'iroll-video-third-quartile'
    | 'iroll-video-completed'
    | 'iroll-skip-ad-request'
    | 'iroll-overlay-close'
  ;

  export class IrollAdEvents {
    static readonly IMP      : IrollAdEvent = 'iroll-impression';
    static readonly PREPARED : IrollAdEvent = 'iroll-prepared';
    static readonly READY    : IrollAdEvent = 'iroll-ready';
    static readonly STARTED  : IrollAdEvent = 'iroll-started';
    static readonly ENDED    : IrollAdEvent = 'iroll-ended';
    static readonly FAILED   : IrollAdEvent = 'iroll-failed';
    static readonly EXPAND   : IrollAdEvent = 'iroll-expand';
    static readonly COLLAPSE : IrollAdEvent = 'iroll-collapse';

    static readonly VIDEO_PROGRESS       : IrollAdEvent = 'iroll-video-progress';
    static readonly VIDEO_STARTED        : IrollAdEvent = 'iroll-video-started';
    static readonly VIDEO_FIRST_QUARTILE : IrollAdEvent = 'iroll-video-first-quartile';
    static readonly VIDEO_MIDPOINT       : IrollAdEvent = 'iroll-video-midpoint';
    static readonly VIDEO_THIRD_QUARTILE : IrollAdEvent = 'iroll-video-third-quartile';
    static readonly VIDEO_COMPLETED      : IrollAdEvent = 'iroll-video-completed';

    static readonly SKIP_AD_REQUEST      : IrollAdEvent = 'iroll-skip-ad-request';
    static readonly OVERLAY_CLOSE        : IrollAdEvent = 'iroll-overlay-close';
  }

  export type PlaybackState = 'stopped' | 'paused' | 'playing';

  export class PlaybackStates {
    static readonly STOPPED: PlaybackState = 'stopped';
    static readonly PLAYING: PlaybackState = 'playing';
    static readonly PAUSED: PlaybackState  = 'paused';
  }

  export type IrollPlaybackRequest
    = 'iroll-request-playback-pause'
    | 'iroll-request-playback-resume'
    | 'iroll-request-playback-restart-on-resume'
  ;

  export class IrollPlaybackRequests {
    static readonly PLAYBACK_PAUSE: IrollPlaybackRequest = 'iroll-request-playback-pause';
    static readonly PLAYBACK_RESUME: IrollPlaybackRequest = 'iroll-request-playback-resume';
    static readonly PLAYBACK_RESTART_ON_RESUME: IrollPlaybackRequest = 'iroll-request-playback-restart-on-resume';

    static isRequest(type: string): boolean {
      return type == IrollPlaybackRequests.PLAYBACK_PAUSE
        || type == IrollPlaybackRequests.PLAYBACK_RESUME
        || type == IrollPlaybackRequests.PLAYBACK_RESTART_ON_RESUME
      ;
    }
  }

  export interface RemoteControlEvent {
    type : RemoteControlEventType;
    keyCode : number;

    preventDefault():void;
    stopPropagation():void;
    stopImmediatePropagation?():void;
  }

  const log = (msg: string, data?: any): void => {
    console.log( msg, data );
  };

  const elA = document.createElement('a');
  export type RemoteControlEventType = 'keydown' | 'keyup';

  class IrollAdWrapper implements IrollAd {
    started: boolean = false;
    disposed: boolean = false;
    position: number = -1;
    duration: number = -1;
    playbackState: PlaybackState = PlaybackStates.STOPPED;
    el: HTMLIFrameElement;
    uuid: string;

    container: HTMLElement;
    tagUrl: string;
    params: IrollAdParameters;
    handler: IrollAdHandler;

    constructor(tagUrl: string, params: IrollAdParameters) {
      this.tagUrl = tagUrl;
      this.params = params;
      this.uuid = randomUUID();
    }

    init(container: HTMLElement, handler?: IrollAdHandler): void {
      this.container = container;
      this.handler = new HandlerWrapper( handler );
    }

    load():void {
      this.log('load()');

      this._createIframe();
    }

    start(): void {
      if (this.started) {
        return;
      }

      this.log('start()');

      this.started = true;
      this.playbackState = PlaybackStates.PLAYING;

      // create iframe
      this._createIframe( true );
      // fire @prepared event
      this._fireDeferredIrollEvent( IrollAdEvents.PREPARED );
      // setup message listener
      window.addEventListener( "message", this._handleIncomingMessage );
    }

    dispose(): void {
      if (this.disposed) {
        return;
      }

      log('dispose()');

      // reset handler
      this.handler = null;
      // remove listener
      window.removeEventListener("message", this._handleIncomingMessage);
      // detach view
      this._disposeIframeEl();
      // mark as disposed
      this.disposed = true;
    }

    stop(): void {
      log('stop()');
    }

    skip(reason: string): void {
      this.log(`skip(reason: ${reason})`);
      this._send2iframe({ type: 'skip', reason: reason });
    }

    injectKeyboardEvent(evt: RemoteControlEvent): void {
      this._send2iframe({ type: evt.type, keyCode: evt.keyCode });
    }

    injectPlaybackInfo(info: PlaybackInfo): void {
      this._send2iframe({ type: 'playback-update', data: info });
    }

    private _send2iframe(data: any): void {
      try {
        this.el.contentWindow.postMessage( data, '*' );
      } catch (e) {
        log('_send2iframe() -- Error! -- ', e);
      }
    }

    private _fireIrollEvent(eType:IrollAdEvent, eData?: any): void {
      this.handler.handleIrollEvent( eType, eData );
    };

    private _fireDeferredIrollEvent(eType:IrollAdEvent, eData?: any): void {
      let id = setTimeout((): void => {
        clearTimeout( id );
        this._fireIrollEvent( eType, eData );
      }, 0);
    };

    private _handleIncomingMessage = (evt:MessageEvent): void => {
      if (!evt.data) {
        return;
      }

      let eType = evt.data['type'] + '';
      let eData = evt.data['params'];

      if (IrollPlaybackRequests.isRequest( eType )) {
        this.handler.handleIrollPlaybackRequest(<IrollPlaybackRequest> eType, eData );
      } else {
        if (eType == IrollAdEvents.VIDEO_PROGRESS && !isNaN(eData['duration'])) {
          this.position = eData['position'];
          this.duration = eData['duration'];
        }

        // this.log(`handleIncomingMessage("${evt.type}", "${eType}", ${JSON.stringify( eData )})`);

        if (eType == IrollAdEvents.ENDED || eType == IrollAdEvents.FAILED) {
          this.playbackState = PlaybackStates.STOPPED;
          window.removeEventListener("message", this._handleIncomingMessage);
        }

        this._fireIrollEvent( <IrollAdEvent> eType, eData );
      }
    };

    private _prepareAdUrl(url: string, autoStart: boolean): string {
      let params = this.params || {};
      params.autoStart = autoStart;

      elA.href = url;
      elA.hash = 'params=' + encodeURIComponent(JSON.stringify( params ));

      return elA.href;
    }

    private _createIframe(autoStart: boolean = false): void {
      this.log('_createIframe()', this.el);

      if (!this.el) {
        this._prepareIframeEl(this._prepareAdUrl( this.tagUrl, autoStart ));
      }
    }

    private _prepareIframeEl(adUrl: string): void {
      let el;

      el = document.createElement('iframe');
      el.setAttribute('className', 'innovid-iroll');
      el.setAttribute('id', this.uuid);
      el.setAttribute('src', adUrl);

      el.style.zIndex     = 1000;
      el.style.border     = '0px';
      el.style.position   = "absolute";
      el.style.background = "transparent";
      el.style.left       = "0px";
      el.style.top        = "0px";
      el.style.width      = "100%";
      el.style.height     = "100%";

      this.container.appendChild( el );

      // save ref
      this.el = el;
    };

    private _disposeIframeEl(): void {
      if (!this.el) {
        return;
      }

      try {
        this.container.removeChild( this.el );
      } catch (e) { /** nothing special */ }

      // reset value
      this.el.setAttribute('src', 'about:blank');
      this.el = null;
    }

    private log(msg: string, data?):void {
      log(`### iroll-wrapper / Ad(${this.uuid}) # ${msg}`, data);
    }
  }

  export function randomUUID (): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c): string => {
      let r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  }

  export interface PlaybackInfo {
    playbackState: PlaybackState;
    position: number;
    startAt: number;
    ended: boolean;
  }

  class HandlerWrapper implements IrollAdHandler {
    private handler: IrollAdHandler;

    constructor(handler?: IrollAdHandler) {
      this.handler = handler;
    }

    handleIrollEvent(type: IrollAdEvent, data?: any): any {
      if (HandlerWrapper._isMethodExists(this.handler, 'handleIrollEvent')) {
        this.handler.handleIrollEvent( type, data );
      }
    }

    handleIrollPlaybackRequest(type: IrollPlaybackRequest, data?: any): any {
      if (HandlerWrapper._isMethodExists(this.handler, 'handleIrollPlaybackRequest')) {
        this.handler.handleIrollPlaybackRequest( type, data );
      }
    }

    private static _isMethodExists(o: Object, methodName: string): Boolean {
      return (o && Object.prototype.toString.apply(o[ methodName ]) == '[object Function]');
    }
  }
}
