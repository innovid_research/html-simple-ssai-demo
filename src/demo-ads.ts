namespace innovid.iroll.demo {
  export const FRAMEWORK_IROLL = 'innovid';

  export class Ads implements SimpleVideoPlayerHandler {
    currentAd: AdInstance;
    currentAds: AdInstance[];
    currentAdPod: AdPod;
    app: DemoApp;

    constructor(app: DemoApp) {
      this.app = app;
    }

    handlePlaybackProgress(position: number): void {
      // this.log(`handlePlaybackProgress(-${position}-)`);

      // notify the current ad about video playback progress
      if (this.currentAd) {
        this.currentAd.processPlaybackStateChanged( this.app.video );
      }

      let adPod;

      if (this.currentAdPod) {
        // check current adPod
        if (position >= (this.currentAdPod.renderTime + this.currentAdPod.duration)) {
          this._resetCurrentAdPod();
        }
      } else {
        // we will initialize AdPod's ads 1 sec before
        adPod = this._getAdPodForPosition( position, 1 );

        if (adPod) {
          this._setupCurrentAdPod( adPod );
        }
      }

      if (this.currentAdPod) {
        this._processCurrentAds(position);
      } else if (this.currentAd && AdTimeUtil.shouldBeStopped( this.currentAd, position )) {
        this._resetAd( this.currentAd )
      }
    }

    handlePlaybackStateChanged(state: PlaybackState, oldState?: PlaybackState): void {

    }

    log(msg: string, data?: any): void {
      console.log(`Demo / Ads # ${msg}`, data);
    }

    private _processCurrentAds(position:number):void {
      for (let ad of this.currentAds) {
        if (AdTimeUtil.shouldBeStarted( ad, position )) {
          this._startAd( ad );
        } else if (AdTimeUtil.shouldBePreloaded( ad, position )) {
          this._preloadAd( ad );
        } else if (AdTimeUtil.shouldBeStopped( ad, position )) {
          this._resetAd( ad );
        }
      }
    }

    private _initAds(adPod: AdPod):void {
      let ads = [];
      let renderTime = adPod.renderTime;

      this.log('_initAds()', adPod);

      for (let adInfo of adPod.ads) {
        let adInstance = this._createAd( adInfo );

        adInstance.startAt = renderTime;
        renderTime += adInstance.duration;

        ads.push( adInstance );
      }

      this.currentAds = ads;
    }

    private _resetCurrentAdPod(): void {
      if (!this.currentAdPod) {
        return;
      }

      this.log('_resetCurrentAdPod()');

      if (this.currentAds) {
        for (let ad of this.currentAds) {
          ad.dispose();
        }
      }

      this.currentAds = null;
      this.currentAdPod = null;
    }

    private _setupCurrentAdPod(adPod: AdPod): void {
      let ads = [];
      let renderTime = adPod.renderTime;

      this.log('_setupCurrentAdPod()', adPod);

      for (let adInfo of adPod.ads) {
        let adInstance = this._createAd( adInfo );

        adInstance.startAt = renderTime;
        renderTime += adInstance.duration;

        ads.push( adInstance );
      }

      this.currentAds = ads;
      this.currentAdPod = adPod;
    }

    private _createAd(adInfo: AdInfo): AdInstance {
      if (adInfo.framework != FRAMEWORK_IROLL) {
        return null;
      }

      return new IrollAdInstance(
        adInfo, this.app.containerEl, this.app.video
      );
    }

    private _startAd(adInstance: AdInstance):void {
      this.log('_startAd()', adInstance);

      this.currentAd = adInstance;
      adInstance.start();
    }

    private _resetAd(adInstance: AdInstance):void {
      this.log('_disposeAd()', adInstance);

      if (adInstance.running) {
        adInstance.stop();
      }
    }

    private _preloadAd(adInstance: AdInstance):void {
      this.log('_preloadAd()', adInstance);

      adInstance.load();
    }

    private _getAdPodForPosition(position:number, offset = 1): AdPod {
      for (let adPod of this.app.cfg.adPods) {
        if ((position >= (adPod.renderTime - offset)) && position < (adPod.renderTime + adPod.duration)) {
          return adPod;
        }
      }

      return null;
    }
  }

  export interface AdInstance {
    started: boolean;
    running: boolean;
    startAt: number;
    duration: number;
    framework: string;

    // info: AdInfo;

    start():void;
    stop():void;
    load():void;
    dispose():void;

    processPlaybackStateChanged(video: SimpleVideoPlayer): void;
  }

  class IrollAdInstance implements AdInstance, IrollAdHandler {
    started: boolean;
    running: boolean;
    startAt: number;
    duration: number;
    framework: string;
    iroll: IrollAd;

    private _container: HTMLElement;
    private _player: SimpleVideoPlayer;
    private _info: AdInfo;

    constructor(info: AdInfo, container: HTMLElement, player: SimpleVideoPlayer) {
      this._container = container;
      this._info = info;
      this._player = player;

      this.duration  = info.duration;
      this.framework = info.framework;
      this.started   = false;
    }

    start(): void {
      this.log('start()');

      if (this.started && this.running) {
        return;
      }

      this.load();

      this.running = true;
      this.started = true;

      this.iroll.start();
    }

    stop(): void {
      this.log('stop()');

      this.running = false;

      if (this.iroll) {
        this.iroll.stop();
      }
    }

    load(): void {
      this.log('load()');

      if (this.started) {
        return;
      }

      this.running = false;
      this.started = true;

      this.iroll = innovid.iroll.create(this._info.companions[0].url, this.getParameters());
      this.iroll.init( this._container, this );
      this.iroll.load();
    }

    dispose(): void {
      this.log('dispose()');
    }

    handleIrollEvent(type: IrollAdEvent, data?: any ): any {
      // app special handling

      switch (type) {
        case IrollAdEvents.OVERLAY_CLOSE:
          window.focus();
          break;
      }
    }

    handleIrollPlaybackRequest(type: IrollPlaybackRequest, data?: any): any {
      switch( type ) {
        case IrollPlaybackRequests.PLAYBACK_RESUME:
          this._player.resume();
          break;
        case IrollPlaybackRequests.PLAYBACK_PAUSE:
          this._player.pause();
          break;
        case IrollPlaybackRequests.PLAYBACK_RESTART_ON_RESUME:
          this._player.requestRestartOnResume();
          break;
      }
    }

    processPlaybackStateChanged(video: SimpleVideoPlayer): void {
      if (this.iroll) {
        this.iroll.injectPlaybackInfo({
          startAt:       this.startAt,
          playbackState: video.playbackState,
          position:      video.position - this.startAt,
          ended:         video.ended
        });
      }
    }

    log(msg: string, data?: any): void {
      console.log(`Demo / Ad # ${msg}`, data);
    }

    getParameters(): IrollAdParameters {
      return {
        keyMap : {
          UP      : 40, // DOWN
          LEFT    : 37, // LEFT
          RIGHT   : 39, // RIGHT
          DOWN    : 38, // UP
          ENTER   : 32, // SPACE
          BACK    : 8   // BACK
        },

        platform : "dev",
        advertisingId : UUID.create()
      };
    }
  }

  class AdTimeUtil {
    static shouldBeStarted(ad:AdInstance, position:number):boolean {
      return ad.started && !ad.running && (position - ad.startAt) >= 0 && position < (ad.startAt + ad.duration);
    }

    static shouldBePreloaded(ad:AdInstance, position:number):boolean {
      if (ad.started || ad.framework != FRAMEWORK_IROLL) {
        return false;
      }

      let dt = ad.startAt - position;

      return dt >= 0 && dt < 1;
    }

    static shouldBeStopped(ad:AdInstance, position:number):boolean {
      return ad.running === true && (position - ad.startAt) > (ad.duration + 1);
    }
  }

  class UUID {
    static create(): string {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c): string => {
        let r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
      });
    }
  }
}
