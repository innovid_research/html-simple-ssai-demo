import PlaybackState = innovid.iroll.PlaybackState;

namespace innovid.iroll.demo {
  export function run(): void {
    (new DemoApp()).run(
      JSON.parse(document.querySelector('#-demo-data-').textContent),
      <HTMLElement> document.querySelector('#-demo-player')
    );
  }

  export class DemoApp {
    private _btnStartEl: HTMLElement;
    private _aEl: HTMLElement;
    private _ads: Ads;

    private _playbackStarted:boolean;

    video: SimpleVideoPlayer;
    videoEl: HTMLVideoElement;
    containerEl: HTMLElement;

    el: HTMLElement;
    cfg: DemoCfg;

    run(cfg:DemoCfg, el: HTMLElement):void {
      console.log('Demo # run()');

      this.cfg = cfg;
      this.el = el;

      this._prepareUI();
      this._preparePlayback();
    }

    private _prepareUI():void {
      console.log('Demo # _prepareUI()');

      this.videoEl = <HTMLVideoElement> this.el.querySelector('#-video');
      this.containerEl = <HTMLDivElement> this.el.querySelector('#-iroll-ad-placeholder');

      this._btnStartEl = <HTMLDivElement> this.el.querySelector('#-btn-start');
      this._btnStartEl.onclick = (): void => {
        this._startPlayback();
      };

      window.addEventListener('keydown', this._handleKeyboardEvent);
      window.addEventListener('focusin', this._handleFocusEvent);
      window.addEventListener('focusout', this._handleFocusEvent);

      this._aEl = <HTMLElement> document.querySelector('#-key-handler-');
      this._aEl.focus();
    }

    private _preparePlayback():void {
      console.log('Demo # _preparePlaylist()');

      this._ads = new Ads( this );

      this.video = new SimpleVideoPlayer( this.videoEl );
      this.video.setup( this.cfg.content, this._ads);
    }

    public _startPlayback():void {
      console.log('Demo # _startPlayback()');

      this._playbackStarted = true;

      // hide start playback button
      this._btnStartEl.style.display = 'none';
      // start playback
      this.video.start();
    }

    private _handleKeyboardEvent = (evt:KeyboardEvent): void => {
      console.log(`Demo # _handleKeydown(${evt.keyCode}, prevented: ${evt.defaultPrevented})`);
      evt.preventDefault();

      if (!this._playbackStarted) {
        this._startPlayback();
      }
    };

    private _handleFocusEvent = (evt: FocusEvent): void => {
      console.log(`Demo # _handleFocusEvent(${evt.type})`, document.activeElement);
    };
  }

  export class SimpleVideoPlayer {
    el: HTMLVideoElement;
    mf: MediaFile;

    playbackState: PlaybackState = PlaybackStates.STOPPED;
    position: number = -1;
    duration: number = -1;

    ended: boolean;
    restartRequested: boolean;
    restarting: boolean;
    restartPosition: number;
    handler: SimpleVideoPlayerHandler;

    private pendingPlayRequest: boolean = false;
    private pendingSeekRequest: boolean = false;

    constructor(el: HTMLVideoElement) {
      this.el = el;
    }

    setup(mf: MediaFile, handler?: SimpleVideoPlayerHandler): void {
      this.log('start()', mf);

      this.mf = mf;
      this.handler = handler;

      this.el.autoplay = false;
      this.el.preload = 'none';
      this.el.loop = false;

      this.position = 0;
      this.duration = mf.duration;

      this.el.src = mf.url;
    }

    start(): void {
      this.log('start()');

      this.el.addEventListener('canplay', this._handleCanPlay);
      this.el.addEventListener('loadmetadata', this._handleCanPlay);
      this.el.addEventListener('timeupdate', this._handlePlaybackProgress);
      this.el.addEventListener('ended', this._handlePlaybackCompleted);
      this.el.addEventListener('pause', this._handlePlaybackStateChanged);
      this.el.addEventListener('play', this._handlePlaybackStateChanged);
      this.el.addEventListener('playing', this._handlePlaybackStateChanged);

      this.pendingPlayRequest = true;
      this.el.load();

      this._setPlaybackState( PlaybackStates.PLAYING );
    }

    resume(): boolean {
      if (this.playbackState != PlaybackStates.PAUSED) {
        return false;
      }

      this.log(`resume() -- is-restart-requested: ${this.restartRequested}`);

      if (this.restartRequested) {
        this._restartPlayback();
      } else {
        this._setPlaybackState( PlaybackStates.PLAYING );
        this.el.play();
      }
    }

    pause(): boolean {
      if (this.playbackState != PlaybackStates.PLAYING) {
        return false;
      }

      this.log(`pause()`);
      this.el.pause();

      this._setPlaybackState( PlaybackStates.PAUSED );

      return true;
    }

    requestRestartOnResume():void {
      if (this.playbackState != PlaybackStates.PAUSED) {
        return;
      }

      this.log(`requestRestartOnResume()`);

      // save position and change flag
      this.restartRequested = true;
      this.restartPosition = this.position;

      // stopping playback w/o changing #playbackState
      this.el.pause(); // ensure
      this.el.removeAttribute('src');
    }

    log(msg:string, data?):void {
      console.log(`Demo / Video # ${msg}`, data);
    }

    private _setPlaybackState(state: PlaybackState): boolean {
      let oldState = this.playbackState;

      if (oldState == state) {
        return false;
      }

      this.playbackState = state;
      this._processPlaybackStateChanged( state, oldState );

      return true;
    }

    private _restartPlayback(): boolean {
      if (this.restarting || !this.restartRequested) {
        return false
      }

      this.log(`_restartPlayback(position: ${this.restartPosition})`);

      this.restarting = true;
      this.restartRequested = false;

      this.pendingPlayRequest = true;

      this.el.src = this.mf.url;
      this.el.load();

      return true;
    }

    private _processPlaybackStateChanged(newState: PlaybackState, oldState?: PlaybackState): void {
      this.log(`processPlaybackStateChanged(${newState}, old: ${oldState}`);

      if (this.handler) {
        this.handler.handlePlaybackStateChanged( newState, oldState );
      }
    }

    private _handleCanPlay = (): void => {
      if (this.pendingPlayRequest === true) {
        this.pendingPlayRequest = false;

        if (this.restarting && this.restartPosition > 0) {
          this.el.currentTime = this.restartPosition;
        } else {
          this.el.play();
        }
      }
    };

    private _handlePlaybackStateChanged = (): void => {

    };

    private _handlePlaybackProgress = (): void => {
      if (this.position == this.el.currentTime) {
        return;
      }

      // !important - ignore playback progress when video is restarting
      if (this.restarting) {
        if (this.playbackState == PlaybackStates.PAUSED && (this.el.currentTime + 0.05) >= this.restartPosition) {
          // reset flags
          this.restarting = false;
          this.restartPosition = -1;
          this.el.play();
        }
      }

      if (!this.restarting) {
        // save position
        this.position = this.el.currentTime;

        // notify handler
        if (this.handler) {
          this.handler.handlePlaybackProgress( this.position );
        }
      }
    };

    private _handlePlaybackCompleted = (): void => {

    }
  }

  export interface SimpleVideoPlayerHandler {
    handlePlaybackProgress(position: number): void;
    handlePlaybackStateChanged(state: PlaybackState, oldState?: PlaybackState):void;
  }

  export interface DemoCfg {
    content: MediaFile,
    adPods: AdPod[]
  }

  export interface MediaFile {
    framework?: string;
    duration: number;
    url: string;
    mimetype: string;
  }

  export interface AdPod {
    renderTime: number;
    duration: number;
    ads: AdInfo[]
  }

  export interface AdInfo {
    id: string;
    duration: number;
    framework?: string,
    companions: CompanionAdInfo[]
  }

  export interface CompanionAdInfo {
    url: string;
    mimetype: string;
  }
}
